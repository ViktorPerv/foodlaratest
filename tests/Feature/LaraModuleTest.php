<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LaraModuleTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * Тест доступности роута
     * @return void
     */
    public function testUserCanBrowseTestPage()
    {
        $response = $this->get('/test');
        $response->assertStatus(200);
    }

    /**
     * Тест добавленя json даннх в БД
     */
    public function testPostFoodData()
    {
        $response = $this->json('POST', '/update', [
            'id' => '1',
            'generic_name' => 'Test Cola',
            'categories' => 'test Categories',
            'image_url' => 'image url'
        ]);

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'success' => true,
            ]);

        $response2 = $this->json('POST', '/update', [
            'id' => '2',
            'generic_name' => 'Test Cola Twin',
            'categories' => 'test Categories',
            'image_url' => 'image url'
        ]);

        $response2
            ->assertStatus(200)
            ->assertExactJson([
                'success' => true,
            ]);

    }

    /**
     * Тест изменения данных, если id этот уже есь
     */
    public function testUpdateFoodData()
    {
        $this->json('POST', '/update', [
            'id' => '1',
            'generic_name' => 'Test Cola',
            'categories' => 'test Categories',
            'image_url' => 'image url'
        ]);

        $response2 = $this->json('POST', '/update', [
            'id' => '1',
            'generic_name' => 'Test Cola 2',
            'categories' => 'test Categories',
            'image_url' => 'image url'
        ]);

        $response2
            ->assertStatus(200)
            ->assertExactJson([
                'success' => true,
            ]);
    }

    /**
     * Проверка что валидация отдает правильный ответ
     */
    public function testUpdateBadFoodData()
    {
        $response = $this->json('POST', '/update', [
            'id' => '1',
            'generic_name' => 'Test Cola 2',
            'categories' => 'test Categories',
            'image_url' => null
        ]);

        $response
            ->assertStatus(422)
            ->assertExactJson([
                'errors' => [
                    'image_url' => ['The image url must be at least 3 characters.']
                ],
                'message' => 'The given data was invalid.'
            ]);
    }

    /**
     * Тест поиска данных
     */
    public function testFoodListData()
    {
        $this->json('POST', '/update', [
            'id' => '1',
            'generic_name' => 'Test Cola',
            'categories' => 'test Categories',
            'image_url' => 'image url'
        ]);

        $this->json('POST', '/update', [
            'id' => '2',
            'generic_name' => 'Test Cola Twin',
            'categories' => 'test Categories',
            'image_url' => 'image url'
        ]);

        $response = $this->json('GET', '/find/Cola');

        $response
            ->assertStatus(200)
            ->assertExactJson([
               [
                   'id' => '1',
                   'generic_name' => 'Test Cola',
                   'categories' => 'test Categories',
                   'image_url' => 'image url'
               ],
                [
                    'id' => '2',
                    'generic_name' => 'Test Cola Twin',
                    'categories' => 'test Categories',
                    'image_url' => 'image url'
                ]
            ]);

        $response2 = $this->json('GET', '/find/test2');

        $response2
            ->assertStatus(200)
            ->assertExactJson([]);
    }
}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- import stylesheet -->
    <link rel="stylesheet" href="//unpkg.com/view-design/dist/styles/iview.css">
    <!-- import iView -->
    <script src="//unpkg.com/view-design/dist/iview.min.js"></script>
</head>
<body>

<div id="app">
    <content>
        <Row type="flex" justify="center" class="code-row-bg">
            <Col span="12">
                <h1>Тестовый модуль:</h1>
            </Col>
        </Row>
        <div style="width:60%; margin: auto; padding-bottom: 50px;">
        <Row justify="space-around" class="code-row-bg">
            <Col span="12">
                <search></search>
            </Col>
        </Row>
        </div>
        <div style="width:60%; margin: auto; padding-bottom: 50px;">
            <Row justify="space-around" class="code-row-bg">
                <Col span="12">
                <data-table></data-table>
                </Col>
            </Row>
        </div>
    </content>
</div>


<script>
    Vue.component('search', {
        template: '<div>' +
            '            <Form :model="formItem" inline>\n' +
            '                <FormItem>\n' +
            '                    <Input v-model="formItem.search" placeholder="Поиск..."></Input>\n' +
            '                </FormItem>\n' +
            '                <FormItem>\n' +
            '                   <Button type="primary" @click="searchItems"  :loading="loading">Поиск</Button>\n' +
            '                </FormItem>' +
            '            </Form>\n' +
            '            <Table :columns="columns" :data="searchData" v-if="show" no-data-text="Пусто">'+
            '            <template slot-scope="{ row }" slot="image">\n' +
            '                <img :src="row.image_url" height="100">\n' +
            '            </template>'+
            '            </Table>'+
            '            </div>',
        data() {
            return {
                formItem: {
                    search: '',
                },
                loading: false,
                searchData: [],
                columns: [
                    {
                        title: 'Наименование',
                        key: 'generic_name'
                    },
                    {
                        title: 'Category',
                        key: 'categories'
                    },
                    {
                        title: 'Изображение',
                        slot: 'image',
                        key: 'image_url'
                    }
                ],
                show: false,
            }
        },
        methods: {
            searchItems() {
                if(this.formItem.search.length >= 3) {
                    this.loading  = true;
                    this.show = false;
                    fetch('/find/'+this.formItem.search)
                        .then((response) => {
                            if(response.ok) {
                                return response.json();
                            }
                            throw new Error('Network response was not ok');
                        })
                        .then((json) => {
                            if (json.length > 0) {
                                this.searchData = json;
                                this.show = true;
                            }
                            else {
                                this.error();
                            }
                        })
                        .finally(() => {
                            this.loading  = false;
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                }
                else {
                    this.warning();
                }

            },
            error() {
                this.$Notice.error({
                    title: 'Поиск',
                    desc:  'Совпадаений по имени не обнаружено'
                });
            },
            warning() {
                this.$Notice.warning({
                    title: 'Поиск',
                    desc:  'Минимум 3 символа'
                });
            },
        }
    });

    Vue.component('data-table', {
        template: '<div>' +
            '<Table :columns="columns1" :data="data1" :loading="loading" no-data-text="Пусто">' +
                '<template slot-scope="{ row, index }" slot="action">\n' +
                '      <Button type="info" @click="writeInBD(index)">Запись в БД</Button>\n' +
                '</template>' +
                '<template slot-scope="{ row }" slot="image">\n' +
                '            <img :src="row.image_url" height="100">\n' +
                '        </template>' +
                '</Table>' +
                '<div style="margin: 10px;overflow: hidden">\n' +
                '      <div style="float: right;">\n' +
                '          <Page :total="total" prev-text="Назад" next-text="Вперед" :page-size="size" @on-change="changePage" />' +
                '      </div>\n' +
                '</div>' +

            '</div>',
        data() {
            return {
                page: 1,
                columns1: [
                    {
                        title: 'Действие',
                        slot: 'action',
                        width: 150,
                        align: 'center'
                    },
                    {
                        title: 'Наименование',
                        key: 'generic_name'
                    },
                    {
                        title: 'Category',
                        key: 'categories'
                    },
                    {
                        title: 'Изображение',
                        slot: 'image',
                        key: 'image_url'
                    }
                ],
                data1: [],
                loading: true,
                total:100,
                size: 20,
            }
        },
        created() {
            this.loadData()
        },
        methods: {
            changePage(page) {
                this.page = page;
                this.loadData();
            },
            loadData() {
                this.loading = true;
                fetch('https://world.openfoodfacts.org/cgi/search.pl?action=process&sort_by=unique_scans_n&page_size=20&json=1&page='+this.page)
                    .then((response) => {
                        if(response.ok) {
                            return response.json();
                        }

                        throw new Error('Network response was not ok');
                    })
                    .then((json) => {
                        this.total = json.count/20;
                        this.data1 = [];
                        json.products.forEach((item) => {
                            const array = [item.product_name, item.generic_name, item.product_name_en, item.product_name_fr];
                            this.data1.push({
                                'id': item.code,
                                'generic_name': array.find((item) => { return item !== ""}),
                                'categories': item.categories,
                                'image_url': item.image_url
                            });
                        })
                    })
                    .finally(() => {
                        this.loading = !this.loading;
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            },
            writeInBD(index) {
                this.updateData(index);
            },
            success () {
                this.$Notice.success({
                    title: 'Запись в БД',
                    desc: 'Успешно, запись добавлена или обновлена'
                });
            },
            error() {
                this.$Notice.error({
                    title: 'Ошибка',
                    desc:  'Бэкенд вернул неожиданный ответ'
                });
            },
            updateData(index) {
                let URL = '/update';
                fetch(URL, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8'
                    },
                    body: JSON.stringify(this.data1[index])
                }).then(response => {
                    if (response.ok) {
                        this.success();
                    }
                    else {
                       this.error();
                    }
                })
                .catch((error) => {
                    console.log(error);
                    this.error();
                });
            }
        }

    });

    new Vue({
        el: '#app',
        data: {
        },

    });


</script>
</body>
</html>

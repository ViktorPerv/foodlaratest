<?php

namespace App\Modules\LaraTestModule\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Item
 * App\LaraTestModule\Models\Item
 *
 * @property string $id
 * @property string $generic_name
 * @property string $categories
 * @property string $image_url
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Modules\LaraTestModule\Models\Item whereGenericName($value)
 * @mixin \Eloquent
 */
class Item extends Model
{
    use HasFactory;


    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'generic_name',
        'categories',
        'image_url'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];


    public static function createItem(array $data)
    {
        $item = self::firstOrNew(['id' => $data['id']]);
        $item->fill($data);
        $status = false;

        \DB::transaction(function() use ($item, &$status) {
            $item->save();
            $status = true;
        });

        return $status;

    }

    public static function findByName(string $name)
    {
        return self::where('generic_name', 'like', '%'.$name.'%')->get();
    }
}

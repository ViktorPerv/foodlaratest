<?php


namespace App\Modules\LaraTestModule\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\LaraTestModule\Models\Item;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TestController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('LaraTestModule::index');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateModuleData(Request $request): JsonResponse
    {
        $validatedData = $this->validate($request, [
            'id' => 'required|max:255',
            'generic_name' => 'min:3|max:255',
            'categories' => 'min:3|max:255',
            'image_url'  => 'min:3|max:255',
        ]);

        if (Item::createItem($validatedData))
        {
            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false], 400);

    }

    /**
     * @param string $name
     * @return JsonResponse
     */
    public function findByName(string $name): JsonResponse
    {
        return response()->json(Item::findByName($name));
    }

}

<?php
use Illuminate\Support\Facades\Route;

Route::group( [ 'namespace' => 'App\Modules\LaraTestModule\Controllers'],
    function(){
        Route::get('/test', ['uses' => 'TestController@index']);
        Route::post('/update', ['uses' => 'TestController@updateModuleData']);
        Route::get('/find/{name}', ['uses' => 'TestController@findByName']);
});

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## О тестовом задании

Вся логика расположена в отдельном модуле - в app/Modules/LaraTestModule

Тесты находятся в LaraModuleTest

Для интерфейса выбрал vue.js и использовал компоненты iview

### Разворачиваем проект

После копирования из репозитория:

 - composer install
 - добавлеяем в .env доступы к локальной БД
 - php artisan migrate
 - php artisan serve
 
 После этого проект доступен по http://127.0.0.1:8000 - по умолчанию сразу редирект на страницу с нужным интерфейсом
 
 #
